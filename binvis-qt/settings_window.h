/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef SETTINGS_WINDOW_H
#define SETTINGS_WINDOW_H

#include <QDialog>

namespace Ui {
class settings_window;
}

class settings_window : public QDialog
{
    Q_OBJECT

public:
    explicit settings_window(QWidget *parent = 0);
    ~settings_window();
    int red,green,blue;
    int orthographical_view;
    int reverse_draw;
    void update_all();
private slots:
    void on_slider_red_valueChanged(int value);
    void on_slider_green_valueChanged(int value);
    void on_slider_blue_valueChanged(int value);
    void on_text_red_textChanged(const QString &arg1);
    void on_text_green_textChanged(const QString &arg1);
    void on_text_blue_textChanged(const QString &arg1);
    void orthographical_view_toggled(bool val);
    void reverse_draw_toggled(bool val);

private:
    Ui::settings_window *ui;
};

#endif // SETTINGS_WINDOW_H
