/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "byte_plot.h"
#include "ui_byte_plot.h"
#include <QMouseEvent>
#include <qdialog.h>
#include <qdebug.h>

byte_plot::byte_plot(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::byte_plot)
{
    ui->setupUi(this);
    data_ptr = NULL;
    this->setPalette(parent->palette());
    sx = 600;
    sy = 400;
    offset = 0;
    rotate = 0;
    flipped = false;
}

byte_plot::~byte_plot()
{
    delete ui;
}

void byte_plot::set_data_container(data_container *data_p)
{
    data_ptr = data_p;
}

void byte_plot::update_image()
{
    for(int n = data_ptr->data_pos; n < data_ptr->size; ++n)
        data_ptr->data_ptr[n] = 0;
    QImage image(data_ptr->data_ptr+offset, sx, sy, QImage::Format_RGB32);
    ui->image_label->setGeometry(0,0, sx, sy);
    ui->image_label->setPixmap(QPixmap::fromImage(image));
    if (flipped) {
        flip_image();
    }
    rotate_image(0);
}

void byte_plot::resizeEvent(QResizeEvent* event) {

    int sx = event->size().width() - 80;
    int sy = event->size().height() - 30;
    if(sx*sy >= DATA_CONTAINER_SIZE/4)
        return;

    this->sx = sx;
    this->sy = sy;
    this->update_image();
    this->update_labels();
}

void byte_plot::update_labels() {
    ui->label_byte_plot_x_size->setText(QString::number(this->sx));
    ui->label_byte_plot_y_size->setText(QString::number(this->sy));
}

void byte_plot::closeEvent(QCloseEvent *event)
{
//    QSettings settings("Binvis");
//    settings.setValue("geometry", saveGeometry());
//    settings.setValue("windowState", saveState());
//    QMainWindow::closeEvent(event);
}

void byte_plot::rotate_button_clicked()
{
        rotate_image(90);
        rotate += 90;
}

void byte_plot::flip_button_clicked()
{
    flip_image();
    flipped = !flipped;
}

void byte_plot::rotate_image(int degrees = 0)
{
    QPixmap pixmap(*ui->image_label->pixmap());
    QMatrix rm;
    if (degrees == 0)
            rm.rotate(rotate);
    else
        rm.rotate(degrees);
    pixmap = pixmap.transformed(rm);
    ui->image_label->setPixmap(pixmap);
}

void byte_plot::reset_image_params()
{
    rotate = 0;
    flipped = false;
    offset = 0;
    set_offset();
}

void byte_plot::flip_image()
{
    QImage image= ui->image_label->pixmap()->toImage();
    QImage flippedImg = image.mirrored(true, false);
    ui->image_label->setPixmap(QPixmap::fromImage(flippedImg));
}


void byte_plot::on_add_offset_clicked()
{
    offset++;
    set_offset();

}

void byte_plot::on_dec_offset_clicked()
{
    offset--;
    set_offset();
}

void byte_plot::set_offset()
{
    if(offset < 0)
        offset = 0;
    this->update_image();
    ui->label_offset->setText(QString::number(offset));
}
