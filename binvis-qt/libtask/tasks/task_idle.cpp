/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "thread"
#include "chrono"

#include "task_idle.h"
#include "../task.h"
#include "../task_manager.h"
#include "task_print.h"

Task_idle::Task_idle(const char *name) : Task(name)
{
}

Task_idle::~Task_idle()
{
}

void Task_idle::execute(Task_manager* manager)
{
	std::this_thread::sleep_for(std::chrono::seconds(1));
}

void* Task_idle::result()
{
	return NULL;
}

void Task_idle::set_executed(bool executed)
{
	this->executed = false;
}
