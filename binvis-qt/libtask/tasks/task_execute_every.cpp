/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "thread"
#include "chrono"
#include "task_execute_every.h"
#include "../task.h"

Task_execute_every::Task_execute_every(std::chrono::milliseconds milliseconds, Task* t, bool high_priority) : Task("Execute every")
{
	this->milliseconds = milliseconds;
	this->t = t;
	this->high_priority = high_priority;
}

void Task_execute_every::execute(Task_manager* manager)
{
	manager->add_task(t);
	std::this_thread::sleep_for(milliseconds);
	manager->add_task(this, high_priority);
}

void* Task_execute_every::result()
{
	return NULL;
}
