#-------------------------------------------------
# Copyright (C) 2015  Niklas Trippler
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = libTask
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    task.cpp \
    task_consumer.cpp \
    task_manager.cpp \
    tasks/task_idle.cpp \
    tasks/task_print.cpp \
    tasks/task_maintenance_cleanup.cpp \
    tasks/task_execute_every.cpp \
    tasks/task_tpm.cpp

HEADERS += \
    task_consumer.h \
    task_manager.h \
    tasks/task_idle.h \
    tasks/task_print.h \
    task.h \
    task_tpm.h \
    tasks/task_maintenance_cleanup.h \
    tasks/task_execute_every.h \
    tasks/task_tpm.h
