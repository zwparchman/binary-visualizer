/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "string.h"
#include "task_byte_plotter.h"
#include "plotter.h"

Task_byte_plotter::Task_byte_plotter() : Task("Task byte plotter")
{
	data = NULL;
	data_cont.data_ptr = NULL;
}

void Task_byte_plotter::set_properties(data_container* data_cont, struct filedatabuffer* fdb, long long start_offset)
{
	if(data_cont == NULL)
		return;
	data_cont->data_pos = (start_offset+fdb->bytes_in_buffer < data_cont->size)? start_offset + fdb->bytes_in_buffer : data_cont->size;
	this->data_cont = *data_cont;
	this->data_length = fdb->bytes_in_buffer;
	this->alignment_max_value = fdb->alignment_max_value;
	this->alignment_scaling = fdb->alignment_scaling;
	this->data = (unsigned char*)malloc(data_length*sizeof(unsigned char));
	memcpy(this->data, fdb->buffer, data_length);

	init_filedatabuffer(fdb->alignments, alignment_max_value, alignment_scaling);
	buffer.byte_plot.data_cont = data_cont;

	this->start_offset = start_offset;
	this->data_cont.data_pos = start_offset;
}

Task_byte_plotter::~Task_byte_plotter()
{
	if(data)
		free(data);
}

void Task_byte_plotter::execute(Task_manager* manager)
{
    if(data == NULL || data_cont.data_ptr == NULL || data_cont.data_pos < 0 || data_cont.data_pos > data_cont.size)
		return;
	long long v;
	while(!no_more_data_in_filedatabuffer(&buffer) && data_cont.data_pos < data_cont.size){
		v = get_next_scaled_value_from_filedatabuffer(&buffer);
		if(v < 0)
			return;
		add_datapoint_to_data_container(v, &data_cont);
	}
}

void Task_byte_plotter::init_filedatabuffer(struct alignment_round* alignment, long long max_value, int scaling)
{
	buffer.position_in_alignment_table = buffer.position_in_file_buffer = 0;
	buffer.alignments = alignment;
	buffer.buffer = data;
	buffer.bytes_in_buffer = data_length;
	buffer.filep = NULL;
	buffer.alignment_max_value = max_value;
	buffer.alignment_scaling = scaling;
}
