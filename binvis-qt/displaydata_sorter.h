/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef DISPLAYDATA_SORTER_H
#define DISPLAYDATA_SORTER_H
#include <string.h>

extern struct point* private_sorter_vertexes;

//Functions for sorting vertices
int compare_display_element_points(const void* first, const void* second);
void sort_displaydata_points(struct displaydata* display_data);
int find_mincount_vertex_number(struct displaydata* display_data, int min_count);

//Functions for sorting indices
int compare_display_element_indices(const void* first, const void* second);
void sort_displaydata_indices(struct displaydata* display_data, size_t indice_count, unsigned int* indices);
int find_mincount_index_number(displaydata *display_data, unsigned int* indices, int min_count, bool reversed);

#endif // DISPLAYDATA_SORTER_H
