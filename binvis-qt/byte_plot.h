/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef BYTE_PLOT_H
#define BYTE_PLOT_H

#include <QDialog>
#include "plotter.h"
namespace Ui {
class byte_plot;
}

class byte_plot : public QDialog
{
    Q_OBJECT

public:
    explicit byte_plot(QWidget *parent = 0);
    ~byte_plot();
    void set_data_container(struct data_container* data_ptr);
    void update_image();
    void reset_image_params();
private:
    Ui::byte_plot *ui;
    struct data_container* data_ptr;
    void update_labels();
    void rotate_image(int degrees);
    void flip_image();
    void set_offset();
    int sx, sy;
    int offset;
    int rotate;
    bool flipped;
protected:
    void resizeEvent(QResizeEvent*);
    void closeEvent(QCloseEvent *event);
private slots:
    void rotate_button_clicked();
    void flip_button_clicked();
    void on_add_offset_clicked();
    void on_dec_offset_clicked();
};

#endif // BYTE_PLOT_H
