/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef PARALLEL_INDICE_SORTER_H
#define PARALLEL_INDICE_SORTER_H

#include "displaydata_sorter.h"
#include "displaydata.h"
class application_logic;
#include <QThread>
/*
 * Worker thread is responsible for sorting a part of an assigned list
 * Requires indices, start_position and stop_position to be set and returns when it is finished sorting
 */
class parallel_indice_sorter_worker : public QThread{
    Q_OBJECT
    public:
    /* Pointer to the indices to be sorted. Must be a pointer to the first element to be sorted */
    void set_indice_pointer(unsigned int* indices);
    /* Start and stop positions in the indice list. Is used to calculate how many elements to sort */
    int start_position, stop_position;
    private:
    /*
     * run() is the thread starting point. It simply calls qsort with the required public parameters and returns as soon as qsort (quicksort) is finished.
     */
    void run();
    unsigned int* indices;
};


/*
 * The master sorter thread.
 * Required set parameters are indices (pointer to the index list to sort), indice_count and display_data
 */
class parallel_indice_sorter_master : public QThread{
    Q_OBJECT
    private:
    //Pointer to the indices to sort
    unsigned int* indices;

    //How many indices are in the indice list
    size_t indice_count;
    //Pointer to display_data structure where vertex data is pulled from
    displaydata* display_data;
    public:
	bool reversed;
    //Set the amount of threads to use while sorting
    void set_thread_count(size_t t);
    void set_indices(unsigned int* indices, size_t indice_count);
    void set_displaydata(displaydata* display_data);
    parallel_indice_sorter_master();
    void set_application_callback(application_logic* logic);
    private:
    //Buffer used for merging results from threads
    unsigned int* sorting_buffer;
    //Pointer to worker threads
    parallel_indice_sorter_worker* threadpool;
    application_logic* application_callback;
    //Master thread entry point. Sets up worker threads and sorts
    void run();
    size_t thread_count;

};
#endif // PARALLEL_INDICE_SORTER_H
