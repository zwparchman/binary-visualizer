
# Copyright (C) 2014  Niklas Trippler and ??yvind Andreas Eide
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



QT      += core gui opengl
CONFIG  += c++11

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets


#Application Info
VERSION = 0.1
QMAKE_TARGET_COPYRIGHT = Niklas Trippler & Øyvind Andreas Eide
QMAKE_TARGET_DESCRIPTION = Visually analyze data and code
#RC_FILE = ../resources/binvis.rc
win32:RC_ICONS += ../resources/binvis_icon.ico

TARGET = binvis-qt
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    glwidget.cpp \
    plotter.cpp \
    displaydata_sorter.cpp \
    parallel_indice_sorter.cpp \
    shannon_entropy.cpp \
    settings_window.cpp \
    constvar/constvar.cpp \
    constvar/bufferex.cpp \
    application_logic.cpp \
    partfile_controller.cpp \
    auxilary_functioons.cpp \
    alignment_tables.cpp \
    tests/alignment_table_tests.cpp \
    byte_plot.cpp \
    libtask/tasks/task_execute_every.cpp \
    libtask/tasks/task_idle.cpp \
    libtask/tasks/task_maintenance_cleanup.cpp \
    libtask/tasks/task_print.cpp \
    libtask/tasks/task_tpm.cpp \
    libtask/task.cpp \
    libtask/task_consumer.cpp \
    libtask/task_manager.cpp \
    task_byte_plotter.cpp \
    global_task_manager.cpp \
    task_data_plotter.cpp \
    ratelimited_task_manager.cpp

HEADERS  += mainwindow.h \
    glwidget.h \
    displaydata.h \
    plotter.h \
    matrix4.h \
    vector3.h \
    displaydata_sorter.h \
    parallel_indice_sorter.h \
    error_messages.h \
    settings_window.h \
    constvar/bufferex.h \
    constvar/constvar.h \
    application_logic.h \
    partfile_controller.h \
    auxilary_functioons.h \
    alignment_tables.h \
    tests/alignment_table_tests.h \
    byte_plot.h \
    libtask/tasks/task_execute_every.h \
    libtask/tasks/task_idle.h \
    libtask/tasks/task_maintenance_cleanup.h \
    libtask/tasks/task_print.h \
    libtask/tasks/task_tpm.h \
    libtask/task.h \
    libtask/task_consumer.h \
    libtask/task_manager.h \
    task_byte_plotter.h \
    global_task_manager.h \
    task_data_plotter.h \
    ratelimited_task_manager.h

FORMS    += mainwindow.ui \
    settings_window.ui \
    byte_plot.ui

OTHER_FILES += \
    vertex_box.glsl \
    vertex_cylinder.glsl \
    fragment.glsl

RESOURCES += \
    ../resources/resources.qrc

shaderCopy.path = $$OUT_PWD
shaderCopy.files = $$OTHER_FILES
INSTALLS += shaderCopy
