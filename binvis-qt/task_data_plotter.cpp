/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <string.h>
#include "task_data_plotter.h"
#include "plotter.h"
//#define LOCKLESS_ADD
Task_data_plotter::Task_data_plotter() : Task("Task data plotter")
{
    data = NULL;
}

Task_data_plotter::~Task_data_plotter()
{
    if(data)
        free(data);
}

void Task_data_plotter::set_properties(struct filedatabuffer *fdb, int x, int y, struct displaydata* display_data, float add_value)
{

    this->data_length = fdb->bytes_in_buffer;
    this->alignment_max_value = fdb->alignment_max_value;
    this->alignment_scaling = fdb->alignment_scaling;
    this->data = (unsigned char*)malloc(data_length*sizeof(unsigned char));
    memcpy(this->data, fdb->buffer, data_length);

    this->buffer = *fdb;
    buffer.buffer = this->data;
    buffer.filep = NULL;
    this->x = x;
    this->y = y;
    this->display_data = display_data;
    this->add_value = add_value;
}

void Task_data_plotter::execute(Task_manager *manager)
{
    union{
        int val;
        struct{ unsigned char w, x, y, z; };
    }chars = {0};
    chars.y = x;
    chars.x = y;
    long long normalized_value;
    while((normalized_value = get_next_scaled_value_from_filedatabuffer(&buffer)) >= 0){
        chars.val = (chars.val | normalized_value) << 8;
#ifndef LOCKLESS_ADD
        add_point_to_displaydata(display_data, add_value, chars.x, chars.y, chars.z);
#else
        add_point_to_displaydata_lockfree(display_data, add_value, chars.x, chars.y, chars.z);
#endif
    }
}
