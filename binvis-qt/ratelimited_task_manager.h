/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "libtask/task_manager.h"

#ifndef RATELIMITED_TASK_MANAGER_H
#define RATELIMITED_TASK_MANAGER_H

class Ratelimited_task_manager : public Task_manager
{
public:
	Ratelimited_task_manager(int max_tasks, int threads);
	void add_task(Task *t, bool high_priority);
private:
	int max_tasks;
};

#endif // RATELIMITED_TASK_MANAGER_H
