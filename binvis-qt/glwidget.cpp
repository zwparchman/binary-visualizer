/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "glwidget.h"
#include "plotter.h"
#include "matrix4.h"
#include "vector3.h"
#include "displaydata_sorter.h"
#include "application_logic.h"
#include <QOpenGLFunctions>
#include <QGLFunctions>
#include <QMouseEvent>
#include <QThread>
#include "error_messages.h"
#define fatal_error printf
#define NUMBER_OF_THREADS_TO_USE_FOR_SORTING 8

#define UNSORTED 0
#define SORTING 1
#define SORTED 2
#define RESORT 3

GLWidget::GLWidget(QWidget *parent) :
    QGLWidget(QGLFormat(QGL::SampleBuffers),parent), vertex_buffer(QGLBuffer::VertexBuffer), index_buffer(QGLBuffer::IndexBuffer)
{
    glFormat.setVersion( 4, 3 );
    glFormat.setProfile( QGLFormat::CoreProfile ); // Requires >=Qt-4.8.0
    glFormat.setSampleBuffers( true );
    mouse_x = 0, mouse_y = 0;
    draw_mode = GL_POINTS;
    connect(this, SIGNAL(do_index_upload()), this, SLOT(upload_indices_to_gpu()), Qt::QueuedConnection);
    connect(this, SIGNAL(do_vertex_upload()), this, SLOT(upload_to_gpu()), Qt::QueuedConnection);
    connect(this, SIGNAL(do_frame_draw()), this, SLOT(update()), Qt::QueuedConnection);
    orthgraphic_view = true;
    reverse_draw = false;
}

GLWidget::~GLWidget()
{

}

void GLWidget::resizeGL(int w, int h)
{
    glViewport( 0, 0, w, qMax( h, 1 ) );
}

void GLWidget::initializeGL()
{
    static bool called = false;
    if(called)
        return;
    called = true;
    INFO("Initializing opengl");
    if(!glFormat.sampleBuffers())
        ERROR("Could not enable sample buffers");
    glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
    init_shaders("vertex_box.glsl", "fragment.glsl");
    upload_indices_to_gpu();
}

void GLWidget::paintGL()
{
    bool l = false;
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if(!shader.bind()){
        ERROR("Could not bind shader");
        return;
    }
    if(!vertex_buffer.bind()){
        ERROR("Could not bind vbo");
        return;
    }
    if(!index_buffer.bind()){
        ERROR("Could not bind ibo");
        return;
    }
    shader.enableAttributeArray("position");
    shader.enableAttributeArray("count");

    if(l){
        for(int x = 0; x < 256; ++x)
            for(int y = 0; y < 256; ++y)
                for(int z = 0; z < 256; ++z)
                    if(display_data->vertices.points[z*256*256 + y*256 + x].count > 0){
                        INFO("Point (%d,%d,%d) has the value: %f", x, y, z, display_data->vertices.points[z*256*256 + y*256 + x].count);
                    }
    }

    Matrix4f rotx = Matrix4f::createRotation(display_data->translations.rotation_x, Vector3f(1.0f, 0.0f, 0.0f));
    Matrix4f roty = Matrix4f::createRotation(display_data->translations.rotation_y, Vector3f(0.0f, 1.0f, 0.0f));

    Matrix4f ortho =Matrix4f::createOrthoPrj(-0.9f, +0.9f, +0.9f, -0.9f, -1.0f, 15.0f);

    rotx.mul(roty);
    ortho.mul(rotx);
    QMatrix4x4 matrix;


    if (orthgraphic_view)
    {
        matrix = QMatrix4x4(ortho.get(), 4, 4);
    }
    else
    {
        matrix = QMatrix4x4(rotx.get(), 4, 4);
    }

    shader.setUniformValue("rotation", matrix);
    shader.setUniformValue("steps", display_data->translations.steps);
    shader.setUniformValue("min_count", display_data->translations.min_count);
    shader.setUniformValue("alpha_value", display_data->translations.alpha);
    shader.setUniformValue("pos_scale", display_data->translations.pos_scale);

    int count = index_buffer.size()/sizeof(unsigned int);
    if(visualization->get_sorting_level() == SORTED){
        //implement reverse draw here
        if(!reverse_draw){
			glDrawElements(draw_mode, visualization->get_draw_count(), GL_UNSIGNED_INT, 0);
		}else{
			glDrawElements(draw_mode, 256*256*256-visualization->get_draw_count(), GL_UNSIGNED_INT, (void*)(visualization->get_draw_count()*sizeof(GLuint)));
        }
    } else {
        glDrawElements(draw_mode, count, GL_UNSIGNED_INT, 0);
    }
    shader.disableAttributeArray("position");
    shader.disableAttributeArray("count");

}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}
QSize GLWidget::sizeHint() const
{
    return QSize(400, 400);
}

bool GLWidget::init_shaders(const char *vert, const char *frag){
    shader.removeAllShaders();
    INFO("Adding vertex shader: %s", vert);
    bool res = shader.addShaderFromSourceFile(QGLShader::Vertex, vert);
    if(!res)
        ERROR("%s", shader.log().toStdString().c_str());
    INFO("Adding fragment shader: %s", frag);
    res = shader.addShaderFromSourceFile(QGLShader::Fragment, frag);
    if(!res)
        ERROR("%s", shader.log().toStdString().c_str());
    INFO("Linking shaders");
    res = shader.link();
    if(!res)
        ERROR("Could not link shader: %s", shader.log().toStdString().c_str());

    request_frame_redraw();
    return res;
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton){
        mouse_x = event->x();
        mouse_y = event->y();
        event->accept();
    }
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    //Ignoring right button for now
    if (event->buttons() == Qt::RightButton) {
        return;
    }

    int delta_x = mouse_x - event->x();
    int delta_y = mouse_y - event->y();
    if (orthgraphic_view) {
        display_data->translations.rotation_y -= delta_x;
    }
    else {
        display_data->translations.rotation_y += delta_x;
    }
    display_data->translations.rotation_x += delta_y;
    mouse_x = event->x();
    mouse_y = event->y();
    event->accept();
    request_frame_redraw();
    emit rotated_signal(display_data->translations.rotation_x, display_data->translations.rotation_y);
}

//Zooming by mousewheel
void GLWidget::wheelEvent(QWheelEvent *event)
{
    float scaleFactor = 0.0005;
    float scale = display_data->translations.pos_scale;
    scale = scale + (event->delta() * scaleFactor);

    if (scale >= 0.0001) {
        set_position_scale_float(scale);
        request_frame_redraw();
        emit wheel_scrolled_signal(scale);
    }
}

void GLWidget::set_step_count(int steps)
{
    display_data->translations.steps = steps;
    request_frame_redraw();
    emit steps_modified_signal(steps);
}

void GLWidget::set_position_scale_float(float value)
{
    display_data->translations.pos_scale = value;
    request_frame_redraw();
}

void GLWidget::set_draw_mode(int index)
{
    GLenum draw_modes[] = {
        GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES, GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY,
        GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY, GL_TRIANGLES_ADJACENCY
        //,GL_PATCHES
    };
    if(index >= 0 && index < (sizeof(draw_modes)/sizeof(draw_modes[0])))
        this->draw_mode = draw_modes[index];
    request_frame_redraw();
}

void GLWidget::toggle_orthographical_view(int val)
{
    orthgraphic_view = (bool) val;
}

void GLWidget::set_displaydata(displaydata *display_data)
{
    this->display_data = display_data;
}

void GLWidget::request_vertex_reupload()
{
    emit do_vertex_upload();
}

void GLWidget::request_indice_reupload()
{
    emit do_index_upload();
}

void GLWidget::request_frame_redraw()
{
    emit do_frame_draw();
}

void GLWidget::set_application_logic_ptr(application_logic *logic)
{
    this->visualization = logic;
}

void GLWidget::set_reverse_draw(bool state)
{
    this->reverse_draw = state;
}

void GLWidget::upload_indices_to_gpu(){
    if(index_buffer.isCreated())
        index_buffer.destroy();
    if(!index_buffer.create())
        ERROR("Failed to create IBO")
                index_buffer.setUsagePattern(QGLBuffer::StaticDraw);
    if(!index_buffer.bind())
        ERROR("Could not bind IBO for upload")
                index_buffer.allocate(visualization->get_index_list(), 256*256*256*sizeof(unsigned int));
    index_buffer.release();
    request_frame_redraw();
}

void GLWidget::upload_to_gpu(){
    if(vertex_buffer.isCreated())
        vertex_buffer.destroy();
    vertex_buffer.create();
    vertex_buffer.setUsagePattern(QGLBuffer::StaticDraw);
    vertex_buffer.bind();
    vertex_buffer.allocate(display_data->vertices.points, sizeof(struct point)*256*256*256);

    shader.setAttributeBuffer("position", GL_FLOAT, 0, 3, sizeof(struct point));
    shader.enableAttributeArray("position");

    shader.setAttributeBuffer("count", GL_FLOAT, sizeof(float)*3, 1, sizeof(struct point));
    shader.enableAttributeArray("count");

    shader.disableAttributeArray("position");
    shader.disableAttributeArray("count");
    vertex_buffer.release();
    request_frame_redraw();
}

void GLWidget::set_point_size(int size){
    glPointSize((float)size);
    request_frame_redraw();
    emit point_size_modified_signal(size);
}

void GLWidget::set_point_alpha(int alpha)
{
    display_data->translations.alpha = ((float)alpha)/10.0f;
    request_frame_redraw();
    emit transperency_modified_signal(alpha);
}

