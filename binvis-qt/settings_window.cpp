/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "settings_window.h"
#include "ui_settings_window.h"
#include "mainwindow.h"

settings_window::settings_window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::settings_window)
{
    ui->setupUi(this);
    QObject::connect(ui->orthographic_checkbox, SIGNAL(clicked(bool)), this, SLOT(orthographical_view_toggled(bool)));
    QObject::connect(ui->reversedraw_checkbox, SIGNAL(clicked(bool)), this, SLOT(reverse_draw_toggled(bool)));
}

settings_window::~settings_window()
{
    delete ui;
}

void settings_window::update_all()
{
    ui->text_red->setText(QString::number(red));
    ui->text_blue->setText(QString::number(blue));
    ui->text_green->setText(QString::number(green));

    ui->slider_red->setValue(red);
    ui->slider_blue->setValue(blue);
    ui->slider_green->setValue(green);
    ui->orthographic_checkbox->setChecked((bool) orthographical_view);
    ui->reversedraw_checkbox->setChecked((bool)reverse_draw);

    QPalette palette = this->palette();
    palette.setColor(QPalette::Window, QColor(red, green, blue, 255));

    this->setPalette(palette);

}

void settings_window::on_slider_red_valueChanged(int value)
{
    red = value;
    update_all();
}

void settings_window::on_slider_green_valueChanged(int value)
{
    green = value;
    update_all();
}

void settings_window::on_slider_blue_valueChanged(int value)
{
    blue = value;
    update_all();
}

void settings_window::on_text_red_textChanged(const QString &arg1)
{
    red = atoi(arg1.toStdString().c_str());
    update_all();
}

void settings_window::on_text_green_textChanged(const QString &arg1)
{
    green = atoi(arg1.toStdString().c_str());
    update_all();
}

void settings_window::on_text_blue_textChanged(const QString &arg1)
{
    blue = atoi(arg1.toStdString().c_str());
    update_all();
}

void settings_window::orthographical_view_toggled(bool val)
{
    orthographical_view = val;
    printf("%d", val);
    update_all();
}

void settings_window::reverse_draw_toggled(bool val)
{
    reverse_draw = val;
    update_all();
}
