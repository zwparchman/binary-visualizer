/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "parallel_indice_sorter.h"
#include "displaydata.h"
#include "displaydata_sorter.h"
#include "error_messages.h"
#include "application_logic.h"
#include <QtCore>
#include <algorithm>
void parallel_indice_sorter_worker::set_indice_pointer(unsigned int *indices)
{
    this->indices = indices;
}

void parallel_indice_sorter_master::set_application_callback(application_logic *logic)
{
    this->application_callback = logic;
}

void parallel_indice_sorter_worker::run()
{
    //using half open intervals so the normal +1 does not apply
    size_t count = (stop_position - start_position);
    qsort((void*)(&(indices[0])), count, sizeof(unsigned int), compare_display_element_indices);
}

parallel_indice_sorter_master::parallel_indice_sorter_master(){
	reversed = false;
    indices = 0;
    threadpool = 0;
}

void parallel_indice_sorter_master::run(){
    if(!indices)
        return;
    if(!threadpool){
        threadpool = new parallel_indice_sorter_worker[thread_count];
    }
    //Set displaydata variable in displaydata_sorter.cpp
    private_sorter_vertexes = display_data->vertices.points;

    for(unsigned int n = 0; n < thread_count; ++n){
        //give out half open intervals [start, stop)
        threadpool[n].start_position = (n*indice_count)/thread_count ;
        threadpool[n].stop_position = ((n+1)*indice_count)/thread_count;
        threadpool[n].set_indice_pointer(indices + threadpool[n].start_position);
    }

    //ensure the last thread picks up the last of the data if it dosen't divide evenly
    threadpool[thread_count-1].stop_position = indice_count;

    //Start all workers
    for(unsigned int n = 0; n < thread_count; ++n)
        threadpool[n].start();

    int positions[thread_count];

    sorting_buffer = (unsigned int*)malloc(indice_count*sizeof(unsigned int));
    if(!sorting_buffer)
        ERROR("Could not allocate memory");

    for(unsigned int n = 0; n < thread_count; ++n){
        positions[n] = threadpool[n].start_position;
    }

    //Wait for all workers to finish
    for(unsigned int n = 0; n < thread_count; ++n)
        while(!threadpool[n].isFinished()){
            this->msleep(10);
        }
    //Merge into list
    unsigned int list_ptr = 0;
    int list_with_highest = -1;
    while(list_ptr < 256*256*256){
        list_with_highest = -1;
        for(int n = 0; n < thread_count; ++n){
            int position = positions[n];
            if(position > threadpool[n].stop_position){
                continue;
            }
            else if(list_with_highest < 0){
                list_with_highest = n;
            }
            else if(display_data->vertices.points[indices[position]].count > display_data->vertices.points[indices[positions[list_with_highest]]].count){
                list_with_highest = n;
            }
        }
        sorting_buffer[list_ptr++] = indices[positions[list_with_highest]++];
    }

	if(!reversed){
		//memcopy to original list and free seperate one
		memcpy(indices, sorting_buffer, 256*256*256*sizeof(unsigned int));
	}else{
		for(int n = 0; n < 256*256*256; ++n)
			indices[n] = sorting_buffer[(256*256*256)-(1+n)];

	}
    free(sorting_buffer);
    if(application_callback)
        application_callback->indice_sortering_finished();
}

void parallel_indice_sorter_master::set_thread_count(size_t t)
{
    this->thread_count = t;
}

void parallel_indice_sorter_master::set_indices(unsigned int *indices, size_t indice_count)
{
    this->indices = indices;
    this->indice_count = indice_count;
}

void parallel_indice_sorter_master::set_displaydata(displaydata *display_data)
{
    this->display_data = display_data;
}
