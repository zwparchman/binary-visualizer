/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include <QTimer>
#include "displaydata.h"
#include "parallel_indice_sorter.h"

#include <QGLBuffer>
#include <QGLShaderProgram>
class application_logic;
class GLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit GLWidget(QWidget *parent = 0);
    ~GLWidget();
    void resizeGL(int, int);
    void initializeGL();
    void paintGL();
    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    bool init_shaders(const char* vertex, const char* fragment);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void set_displaydata(displaydata* display_data);
    void request_vertex_reupload();
    void request_indice_reupload();
    void request_frame_redraw();
    void set_application_logic_ptr(application_logic* logic);
    void set_reverse_draw(bool state);
public slots:
    void upload_to_gpu();
    void upload_indices_to_gpu();
    void set_point_size(int size);
    void set_point_alpha(int alpha);
    void set_step_count(int steps);
    void set_position_scale_float(float value);
    void set_draw_mode(int index);
    void toggle_orthographical_view(int val);
signals:
    void do_vertex_upload();
    void do_index_upload();
    void do_frame_draw();
    void wheel_scrolled_signal(float);
    void point_size_modified_signal(int);
    void transperency_modified_signal(float);
    void steps_modified_signal(int);
    void rotated_signal(float x, float y);

protected:
    virtual void wheelEvent(QWheelEvent* event);
private:
    QGLShaderProgram shader;
    QGLBuffer vertex_buffer;
    QGLBuffer index_buffer;
    QGLFormat glFormat;
    GLenum draw_mode;
    int mouse_x, mouse_y;
    displaydata* display_data;
    application_logic* visualization;
    bool orthgraphic_view;
    bool reverse_draw;
};

#endif // GLWIDGET_H
